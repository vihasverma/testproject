# 4 Step Registration  Web App
>Submitted By: Vihas Verma

Created a multi step registartion process with following features


  - 4 Step Registration

  - Payment Gateway integrated (Demo Url)

  - Store User information in database in each step and save payment ID after completion (respone of demo api)

  - user can leave the registration on every step/view until he finished the whole

registration successfully.

  - user is redirected to the last opened step when he’s joining the

process again.

  - Basic html validation


### Installation



Install the dependencies and start the server. During composer install command enter the demo payment api url: https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data

 


```sh

$ git clone https://bitbucket.org/vihasverma/testproject.git

$ cd testproject

$ composer install

$ iban_api: https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data

$ php app/console server:run

```


##URL for registration: (/register)


http://127.0.0.1:8000/register 
