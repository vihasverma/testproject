<?php
namespace RegistrationBundle\Services\PaymentGateway;


class PaymentContext {

    private $strategy = null;
    public function __construct(PaymentGatewayInterface $paymentGateway) {
        $this->strategy = $paymentGateway;
    }
    public function pay() {
        return $this->strategy->pay();
    }
}