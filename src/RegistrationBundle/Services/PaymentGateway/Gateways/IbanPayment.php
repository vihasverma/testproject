<?php
namespace RegistrationBundle\Services\PaymentGateway\Gateways;


use GuzzleHttp\Client;
use RegistrationBundle\Services\PaymentGateway\PaymentGatewayInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class IbanPayment implements PaymentGatewayInterface {

    private $request;
    private $client;
    private $apiUrl;
    public function __construct(RequestStack $request, Client $client, $apiUrl) {
        $this->request = $request->getCurrentRequest();
        $this->client = $client;
        $this->apiUrl =$apiUrl;

    }
    public function pay() {

        $postData = $this->request->get('RegistrationStep3Form');
        $response = $this->client->post($this->apiUrl
            , [
            'content-type' => 'application/json',
            'body' =>json_encode(
                [
                    'customerId' => 1,
                    'owner' => $postData['owner'],
                    'iban' => $postData['iban']
                ])
        ]);
        return $response;
    }
}