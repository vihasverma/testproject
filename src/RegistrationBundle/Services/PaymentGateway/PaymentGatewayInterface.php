<?php
namespace RegistrationBundle\Services\PaymentGateway;

interface PaymentGatewayInterface {
    public function pay();

}