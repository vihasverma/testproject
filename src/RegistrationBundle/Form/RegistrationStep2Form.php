<?php
namespace RegistrationBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationStep2Form extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('street')
            ->add('housenumber')
            ->add('zip')
            ->add('city');
    }

    public function getBlockPrefix()
    {
        return 'RegistrationStep2Form';
    }

}