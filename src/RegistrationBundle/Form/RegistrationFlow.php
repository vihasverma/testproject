<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 10/6/2018
 * Time: 10:47 AM
 */

namespace RegistrationBundle\Form;


use Craue\FormFlowBundle\Form\FormFlow;
use Doctrine\ORM\EntityManager;
use RegistrationBundle\Entity\Steps;
use RegistrationBundle\Entity\User;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\VarDumper\VarDumper;

class RegistrationFlow extends FormFlow
{
    /** @var  EntityManager $em */
    private $em;
    /** @var  Request $request */
    public $request;

    public function __construct(EntityManager $em, RequestStack $request)
    {
        $this->em = $em;
        $this->request = $request->getCurrentRequest();
    }

    /**
     * This is the configuration setting for the registration steps
     * @return array
     */
    protected function loadStepsConfig()
    {
        return array(
            array(
                'label' => 'Personal Information',
                'form_type' => 'RegistrationBundle\Form\RegistrationStep1Form'
            , 'skip' => ($this->skipStep() >= 1)
            )

        ,
            array(
                'label' => 'Address Information',
                'form_type' => 'RegistrationBundle\Form\RegistrationStep2Form'
            , 'skip' => ($this->skipStep() >= 2)
            ),
            array(
                'label' => 'Payment Information',

                'form_type' => 'RegistrationBundle\Form\RegistrationStep3Form'
            )
        );
    }

    /**
     * @param $formData
     * Save the information in the database at each step completion
     */
    public function save($formData)
    {

        $steps = $this->userStep();
        $currentStep = $this->request->request->get('flow_registration_step');
        $userToken = $this->request->cookies->get('userToken');
        if (count($steps) == 0) {
            $this->em->persist($formData);
            $this->em->flush();
            $step = new Steps();
            $step->setUserId($formData->getId())->setToken($userToken);
        } else {
            $step = $steps[0];
        }

        $step->setStep($currentStep);
        $this->em->persist($step);
        $this->em->flush();


    }

    /**
     * @return null|object|User
     */
    public function formData()
    {
        $userStep = $this->userStep();
        if (count($userStep) > 0) {

            $formData = $this->em->getRepository('RegistrationBundle:User')->find(array("id" => $userStep[0]->getUserId()));
        } else {
            $formData = new User();
        }
        return $formData;
    }

    /**
     * @return array
     * This is used when user return the site. User is redirected to the last step
     */
    protected function userStep()
    {
        $userToken = $this->request->cookies->get('userToken');
        $steps = $this->em->getRepository('RegistrationBundle:Steps')->findBy(array("token" => $userToken));
        return $steps;
    }

    /**
     * Current step of the user
     * @return int
     */
    protected function skipStep()
    {
        $userStep = $this->userStep();
        if (count($userStep) > 0) {
            return $userStep[0]->getStep();
        } else {
            return 0;
        }
    }


}