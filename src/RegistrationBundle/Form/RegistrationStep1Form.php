<?php
namespace RegistrationBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationStep1Form extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('firstname')
            ->add('lastname')->add('telephone');
    }

    public function getBlockPrefix()
    {
        return 'RegistrationStep1Form';
    }

}