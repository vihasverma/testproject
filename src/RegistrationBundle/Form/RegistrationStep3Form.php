<?php
namespace RegistrationBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationStep3Form extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('owner', 'text', array('mapped' => false))
            ->add('iban', 'text', array('mapped' => false)
            );
    }

    public function getBlockPrefix()
    {
        return 'RegistrationStep3Form';
    }

}