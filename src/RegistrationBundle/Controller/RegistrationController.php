<?php

namespace RegistrationBundle\Controller;

use RegistrationBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\VarDumper\VarDumper;

class RegistrationController extends Controller
{
    /**
     * @Route("/register")
     *
     */
    public function indexAction()
    {
        $confirmMessage = "";
        $session = $this->get('session');
        $session->start();
        $flow = $this->get('registration.form');
        // Get the filled input field of the step
        $formData = $flow->formData();
        $flow->bind($formData);
        $form = $flow->createForm();
        if ($flow->isValid($form)) {
            if ($flow->nextStep()) {
                $form = $flow->createForm();
            } else {
                // Payment process
                $response = $this->get('payment_method')->pay();
                $responseBody = json_decode($response->getBody()->getContents());
                if ($response->getStatusCode() == 200 && $responseBody->paymentDataId != "") {
                    $confirmMessage = "Payment Successful. Transaction Id:" . $responseBody->paymentDataId;
                    $formData->setStatus(1);
                    $formData->setPaymentId($responseBody->paymentDataId);
                    // Save the user detail at each step
                    $flow->save($formData);
                    $response = $this->render('@Registration/registration.html.twig', array(
                        'form' => $form->createView(),
                        'flow' => $flow,
                        'confirm_message' => $confirmMessage
                    ));
                    $session->invalidate();
                    $response->headers->setCookie(new Cookie('userToken', $session->getId(), -100));
                    return $response;
                } else {
                    // payment gateway fail
                    $form->addError(new FormError($responseBody->message));

                }
            }
            $flow->save($formData);
        }

        $response = $this->render('@Registration/registration.html.twig', array(
            'form' => $form->createView(),
            'flow' => $flow,
            'confirm_message' => $confirmMessage
        ));
        if (null == $flow->request->cookies->get('userToken')) {
            $response->headers->setCookie(new Cookie('userToken', $session->getId(), time() + 3600 * 24 * 7));
        }
        return $response;
    }
}
